/* eslint-disable import/order */
/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
/* eslint-disable consistent-return */
/* eslint-disable func-names */
/* eslint-disable no-console */

/**
 * This module work as translator between udp protocol and Browser WebSocket.
 * get browser time request and send ntp time server as the response
 */
const net = require('net');
const ntp = require('./index');
const http = require('http');
const WebSocketServer = require('websocket').server;
const configManager = require('./configManager');

let server = null;
let wsServer = null;

/**
 *
 * @param {*} reqeustConnection Webscoket client connection
 * @param {*} response time server response
 */
function sendNtpTimeToWebSokcetClient(reqeustConnection, response) {
  reqeustConnection.sendUTF(response.time);
}

function getTimeFromNtpServer(reqeustConnection, cbk) {
  /* ntp client. use udp socket to connect server */
  ntp(function(err, response) {
    if (err) return console.error(err);
    cbk(reqeustConnection, response);
  });
}

/** WebScoketServer onMessage call back */
function onMessage(request) {
  const connection = request.accept(null, request.origin);
  connection.on('message', function(message) {
    console.log('Received Message:', message.utf8Data);
    getTimeFromNtpServer(connection, sendNtpTimeToWebSokcetClient);
  });
  connection.on('close', function(reasonCode, description) {
    console.log('Client has disconnected.');
  });
}

/** Start WebSocketServer */
function startSocketServer() {
  server = http.createServer();
  function afterReadConfig(config) {
    server.listen(config.port);
    wsServer = new WebSocketServer({
      httpServer: server,
    });
    wsServer.on('request', onMessage);
  }
  configManager.readSocketServerConfig(afterReadConfig.bind(this));
}

module.exports = startSocketServer;
