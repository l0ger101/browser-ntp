/* eslint-disable func-names */
/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
const fs = require('fs');
const path = require('path');

function readConfigFile(cbk) {
  fs.readFile(`${path.dirname(__filename)}/conf.json`, 'utf8', cbk);
}

function readNtpServerConfig(afterReadCbk) {
  readConfigFile(function(error, data) {
    if (error) {
      throw err;
    }
    const config = JSON.parse(data);
    const ntpServerConfig = config.ntpServers[0];
    return afterReadCbk(ntpServerConfig);
  });
}

function readSocketServerConfig(afterReadCbk) {
  readConfigFile(function(error, data) {
    if (error) {
      throw err;
    }
    const config = JSON.parse(data);
    const socketServerConfig = config.socketServer;
    return afterReadCbk(socketServerConfig);
  });
}

module.exports = {
  readNtpServerConfig,
  readSocketServerConfig,
};
